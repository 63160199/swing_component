/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.swing_component;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


/**
 *
 * @author Kitty
 */
public class Labelep2 extends Frame implements ActionListener {
 JTextField field; JLabel label; JButton button;
    Labelep2(){
        field = new JTextField();
        field.setBounds(50, 50, 150, 20);
        label = new JLabel();
        label.setBounds(50, 100, 250, 20);
        button = new JButton("Find IP");
        button.setBounds(50, 150, 95, 30);
        button.addActionListener(this);
        add(field);add(label);add(button);
        this.setSize(300, 400);
        this.setLayout(null);
        this.setLocationRelativeTo(null);
        setVisible(true);
    }
    public static void main(String[] args) {
        new Labelep2();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       try{
           String host = field.getText();
           String ip = java.net.InetAddress.getByName(host).getHostAddress();
           label.setText("IP OF "+host+" IS "+ip);
       }catch(Exception ex){
           System.out.println(ex);
       }
    }
      
   
}
   
}
